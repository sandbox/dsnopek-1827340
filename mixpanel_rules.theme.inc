<?php

function theme_mixpanel_rules_properties_form($element) {
  $header = array(t('Name'), t('Value'));
  $rows = array();

  foreach (element_children($element) as $key) {
    $item =& $element[$key];
    
    $row = array();
    foreach (array('name', 'value') as $name) {
      unset($item[$name]['#title']);
      $row[] = drupal_render($item[$name]);
    }

    $rows[] = $row;
  }

  return theme('table', $header, $rows, array('id' => 'mixpanel-rules-properties-table')) . drupal_render($element);
}

