<?php

/**
 * Implementation of hook_rules_action_info().
 */
function mixpanel_rules_rules_action_info() {
  $actions = array();
  $actions['mixpanel_rules_track'] = array(
    'label' => t('Track event on Mixpanel'),
    'arguments' => array(
      'event' => array(
        'type' => 'string',
        'label' => 'Event name',
      ),
      'account' => array(
        'type' => 'user',
        'label' => t('User who will be recorded as triggering the event'),
      ),
    ),
    'eval input' => array(),
    'module' => 'Mixpanel',
  );

  // Allow tokens or PHP to be in the property values
  for ($i = 0; $i < MIXPANEL_RULES_PROPERTIES_MAX; $i++) {
    $actions['mixpanel_rules_track']['eval input'][] = "properties|$i|value";
  }

  return $actions;
}

function mixpanel_rules_track_form($settings, &$form) {
  $form['settings']['properties'] = array(
    '#type' => 'fieldset',
    '#title' => t('Properties'),
    '#description' => t('If you have the <em>token</em> module enabled, you can use tokens in property values but NOT names.'),
    '#tree' => TRUE,
    '#theme' => 'mixpanel_rules_properties_form',
  );

  for ($i = 0; $i < MIXPANEL_RULES_PROPERTIES_MAX; $i++) {
    $element = array();
    $element['name'] = array(
      '#type' => 'textfield',
      '#title' => t('Property @num name', array('@num' => $i + 1)),
      '#default_value' => $settings['properties'][$i]['name'],
    );
    $element['value'] = array(
      '#type' => 'textfield',
      '#title' => t('Property @num value', array('@num' => $i + 1)),
      '#default_value' => $settings['properties'][$i]['value'],
    );

    $form['settings']['properties'][$i] = $element;
  }
}

function mixpanel_rules_track($event, $account, $settings) {
  // build the actual property list
  $properties = array();
  foreach ($settings['properties'] as $prop) {
    $prop['name'] = trim($prop['name']);
    if (!empty($prop['name'])) {
      $properties[$prop['name']] = trim($prop['value']);
    }
  }

  mixpanel_track($event, $properties, $account);
}

